package com.cdac.security;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	/*
	 * Configure Authentication Manager
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		/*
		 * By default spring comes with 2 Authentication providers :
		 * 
		 * 1. JDBC Authentication Provider
		 * 
		 * 2. In-Memory Authentication Provider
		 */

		/*
		 * In-Memory Authentication Provider (H2 Database)
		 */

		/*
		 * With Role
		 * 
		 * Using authorities() method
		 */

//		auth.inMemoryAuthentication().withUser("admin").password("{noop}admin").authorities("ROLE_ADMIN");

		/*
		 * With Role
		 * 
		 * Using roles() method
		 */

//		auth.inMemoryAuthentication().withUser("admin").password("{noop}admin").roles("ADMIN");

		/*
		 * Without Role
		 * 
		 */
//		auth.inMemoryAuthentication().withUser("admin").password("{noop}admin").roles();
		
		/*
		 * With Role
		 * 
		 * Multiple Users/Authorities
		 */

		auth.inMemoryAuthentication().withUser("admin").password("{noop}admin").roles("ADMIN").and().withUser("shivamp")
				.password("{bcrypt}$2a$10$HOarHDalHW4sVnq2dIJmTem320RzAKjzNsCN3D7ofMPsp0UEj4H9y").roles("USER");

		/*
		 * {noop} : It will select NoPasswordEncoder
		 * 
		 * {bcrypt} : It will select BcryptPasswordEncoder
		 * 
		 * Bcrypt Password Encoded String for 'shivamp' ->
		 * $2a$10$HOarHDalHW4sVnq2dIJmTem320RzAKjzNsCN3D7ofMPsp0UEj4H9y
		 */

		/*
		 * If we provide role as ROLE_ADMIN then spring gives exception
		 * 
		 * java.lang.IllegalArgumentException: ROLE_ADMIN cannot start with ROLE_ (it is
		 * automatically added)
		 */

	}

}
